public class Account {
    private String id;
    private String name;
    private int balance;

    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }

    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public String getID() {
        return this.id;
    }

    public String getName() {
        return this.name;
    }

    public int getBalance() {
        return this.balance;
    }

    public int credit(int amount) {
        this.balance += amount;
        return amount;
    }

    public int debit(int amount) {
        if (this.balance < amount) {
            System.out.println("Amount Exceeded!");
            return 0;
        }
        return this.balance -= amount;
    }

    public int transferTo(Account c, int amount) {
        if (this.balance < amount) {
            System.out.println("Amount Exceeded!");
            return 0;
        }
        this.balance -= amount;
        c.balance += amount;
        return this.balance;
    }

    public String toString() {
        return "Account[id=" + this.id + " name : " + getName() + " balance : " + getBalance() + "]";
    }

}
