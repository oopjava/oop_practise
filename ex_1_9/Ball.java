public class Ball {
    private float x;
    private float y;
    private float xDelta;
    private float yDelta;
    private int radius;

    public Ball(float x, float y, int radius, float xDelta, float yDelta) {
        this.x = x;
        this.y = y;
        this.radius = radius;
        this.xDelta = xDelta;
        this.yDelta = yDelta;
    }

    public float getX() {
        return this.x;
    }

    public void setX(float x) {
        this.x = x;
    }

    public float getY() {
        return this.y;
    }

    public void setY(float y) {
        this.y = y;
    }

    public int getRadius() {
        return this.radius;
    }

    public void setRadius(int radius) {
        this.radius = radius;
    }

    public float getXdelta() {
        return this.xDelta;
    }

    public void setXdelta(float xDelta) {
        this.xDelta = xDelta;
    }

    public float getYdelta() {
        return this.yDelta;
    }

    public void setYdelta(float yDelta) {
        this.yDelta = yDelta;
    }

    public void move() {
        this.x = getX() + getXdelta();
        this.y = getY() + getYdelta();
    }

    public void reflectHorizontal() {
        this.xDelta -= getXdelta();
    }

    public void reflectVertical() {
        this.yDelta -= getYdelta();
    }

    public String toString() {
        return "Ball [(" + getX() + "," + getY() + "),speed=" + getXdelta() + "," + getYdelta() + ")]";
    }

}
