public class TestMain {
    public static void main(String[] args) {

        MyTriangle t1 = new MyTriangle(0, 0, 3, 0, 0, 3);
        System.out.println(t1);
        System.out.println(t1.getPerimeter());
        System.out.println(t1.getType());

        // Isoscles triangle
        MyPoint v1 = new MyPoint(-2, 3);
        MyPoint v2 = new MyPoint(-5, -4);
        MyPoint v3 = new MyPoint(2, -1);

        MyTriangle t2 = new MyTriangle(v1, v2, v3);
        System.out.println(t2);
        System.out.println(t2.getPerimeter());
        System.out.println(t2.getType());

    }
}