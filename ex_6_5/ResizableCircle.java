public class ResizableCircle extends Circle implements Resizable {
    public ResizableCircle(double radius) {
        super(radius);
    }

    public void resize(int percent) {
        double tmp = super.radius;
        tmp *= percent / 100.0;
        super.radius+=tmp;

    }

    @Override
    public String toString() {
        return "ResizableCircle[" + super.toString() + "]";
    }
}
