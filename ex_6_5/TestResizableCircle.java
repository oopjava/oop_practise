public class TestResizableCircle {
    public static void main(String[] args) {
        ResizableCircle rc = new ResizableCircle(5.5);
        System.out.println(rc);
        System.out.println(rc.radius);
        System.out.println(rc.getArea());
        System.out.println(rc.getPerimeter());
        rc.resize(5);
        System.out.println("After Resizing value");
        System.out.println(rc.radius);
        System.out.println(rc.getArea());
        System.out.println(rc.getPerimeter());

    }
}
