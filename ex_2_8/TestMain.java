public class TestMain {
    public static void main(String[] args) {

        // null object reference
        MyCircle c1 = new MyCircle();
        System.out.println(c1);
        System.out.println(c1.getArea());
        System.out.println(c1.getCircumference());

        // value cosntructor
        MyCircle c2 = new MyCircle(2, 2, 3);
        System.out.println(c2);
        System.out.println(c2.getArea());
        System.out.println(c2.getCircumference());

        // object constructor
        MyPoint newCircle = new MyPoint(1, 1);
        MyCircle c3 = new MyCircle(newCircle, 2);
        System.out.println(c3);
        System.out.println(c3.getArea());
        System.out.println(c3.getCircumference());

        // Distance MEasurement
        System.out.println("Circle1 - Circle2 distance : %.2f" + c1.distance(c2));  // problem with format specifier
        System.out.println("Circle2 - Circle3 distance : %0.3f" + c2.distance(c3));
        System.out.println("Circle2 - Circle1 distance : " + c2.distance(c1));

    }
}