public class MyPoint {

    private int x;
    private int y;

    public MyPoint() {
        this.x = 0;
        this.y = 0;
    }

    public MyPoint(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public int getX() {
        return this.x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return this.y;
    }

    public void setY(int y) {
        this.y = y;
    }

    public int[] getXY() {
        int[] point = { this.x, this.y };
        return point;
    }

    public void setXY(int x, int y) {
        this.x = x;
        this.y = y;
    }

    public String toString() {
        return "(" + this.x + "," + this.y + ")";
    }

    public double distance(int x, int y) {
        int p = Math.abs(this.x - x);
        int q = Math.abs(this.y - y);
        double r = Math.sqrt((p * p) + (q * q));
        return r;
    }

    public double distance(MyPoint myPoint) {
        int p = Math.abs(this.x - myPoint.getX());
        int q = Math.abs(this.y - myPoint.getY());
        double r = Math.sqrt((p * p) + (q * q));
        return r;
    }

    public double distance() {
        int p = Math.abs(this.x - 0);
        int q = Math.abs(this.y - 0);
        double r = Math.sqrt((p * p) + (q * q));
        return r;
    }

}
