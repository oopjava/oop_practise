public class TestMyTime {
    public static void main(String[] args) {
        MyTime m1 = new MyTime();
        System.out.println(m1);

        System.out.println("Next Second Test");
        MyTime m2 = new MyTime(8, 57, 33);
        System.out.println(m2);
        System.out.println(m2.next_second());
        System.out.println(m2.next_second());
        System.out.println(m2.next_second());

        MyTime m3 = new MyTime(23, 59, 58);
        System.out.println(m3);
        System.out.println(m3.next_second());
        System.out.println(m3.next_second());
        System.out.println(m3.next_second());

        System.out.println("Next Minute Test");
        MyTime m4 = new MyTime(23, 58, 58);
        System.out.println(m4);
        System.out.println(m4.next_minute());
        System.out.println(m4.next_minute());
        System.out.println(m4.next_minute());

        System.out.println("Next Hour Test");
        MyTime m5 = new MyTime(22, 58, 58);
        System.out.println(m5);
        System.out.println(m5.next_hour());
        System.out.println(m5.next_hour());
        System.out.println(m5.next_hour());
        System.out.println(m5.next_hour());
        System.out.println(m5.next_hour());

        System.out.println("Previous Second Test");
        MyTime m6 = new MyTime(1, 1, 2);
        System.out.println(m6);
        System.out.println(m6.previous_second());
        System.out.println(m6.previous_second());
        System.out.println(m6.previous_second());
        System.out.println(m6.previous_second());
        System.out.println(m6.previous_second());

        System.out.println("Previous Minute Test");
        MyTime m7 = new MyTime(1, 1, 2);
        System.out.println(m7);
        System.out.println(m7.previous_minute());
        System.out.println(m7.previous_minute());
        System.out.println(m7.previous_minute());
        System.out.println(m7.previous_minute());
        System.out.println(m7.previous_minute());

        System.out.println("Previous Hour Test");
        MyTime m8 = new MyTime(1, 1, 2);
        System.out.println(m8);
        System.out.println(m8.previous_hour());
        System.out.println(m8.previous_hour());
        System.out.println(m8.previous_hour());
        System.out.println(m8.previous_hour());
        System.out.println(m8.previous_hour());
        System.out.println(m8.previous_hour());

    }
}
