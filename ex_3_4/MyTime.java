import java.time.LocalTime;

public class MyTime {
    private int hour;
    private int minute;
    private int second;

    public MyTime() {
        this.hour = 0;
        this.minute = 0;
        this.second = 0;
    }

    public MyTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }

    public int getSecond() {
        return this.second;
    }

    public void setHour(int hour) {
        if (hour <= 23 && hour >= 0) {
            this.hour = hour;
        } else {
            throw new IllegalArgumentException("Invalid hour!");
        }
    }

    public void setMinute(int minute) {
        if (minute <= 59 && minute >= 0) {
            this.minute = minute;
        } else {
            throw new IllegalArgumentException("Invalid minute!");
        }
    }

    public void setSecond(int second) {
        if (second <= 59 && second >= 0) {
            this.second = second;
        } else {
            throw new IllegalArgumentException("Invalid second!");
        }
    }

    public void setTime(int hour, int minute, int second) {

        if (hour <= 23 && hour >= 0) {
            this.hour = hour;
        } else if (minute >= 0 && minute <= 59) {
            this.minute = minute;
        } else if (second >= 0 && second <= 59) {
            this.second = second;
        } else {
            throw new IllegalArgumentException("Invalid hour, minute, or second!");
        }

    }

    public String toString() {

        // moder java Time package
        LocalTime time = LocalTime.of(this.hour, this.minute, this.second);
        return time.toString();

    }

    public MyTime next_second() {

        if (this.second == 59) {
            if (this.minute == 59) {
                if (this.hour == 23) {
                    this.hour = 0;
                    this.minute = 0;
                    this.second = 0;
                } else {
                    ++this.hour;
                    this.minute = 0;
                    this.second = 0;
                }
            } else {
                ++this.minute;
                this.second = 0;
            }
        } else
            ++this.second;

        return this;
    }

    public MyTime next_minute() {

        if (this.minute == 59) {
            if (this.hour == 23) {
                this.hour = 0;
                this.minute = 0;
                this.second = 0;
            } else {
                ++this.hour;
                this.minute = 0;
                this.second = 0;
            }
        } else {
            ++this.minute;
        }

        return this;
    }

    public MyTime next_hour() {

        if (this.hour == 23) {
            this.hour = 0;
            this.minute = 0;
            this.second = 0;
        } else {
            ++this.hour;
        }

        return this;
    }

    public MyTime previous_second() {

        if (this.second == 0) {
            if (this.minute == 0) {
                if (this.hour == 0) {
                    this.hour = 23;
                    this.minute = 59;
                    this.second = 59;
                } else {
                    --this.hour;
                    this.minute = 59;
                    this.second = 59;
                }
            } else {
                --this.minute;
                // this.second = 59;
            }
        } else
            --this.second;

        return this;
    }

    public MyTime previous_minute() {

        if (this.minute == 0) {
            if (this.hour == 0) {
                this.hour = 23;
                this.minute = 59;
            } else {
                this.minute = 59;
                --this.hour;
            }
        } else {
            --this.minute;
            // this.second = 59;
        }

        return this;
    }

    public MyTime previous_hour() {

        if (this.hour == 0) {
            this.hour = 23;
            // this.minute = 59;
        } else {
            --this.hour;
            // this.second = 59;
        }

        return this;
    }

}
