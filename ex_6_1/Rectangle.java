public class Rectangle extends Shape {
    protected double length;
    protected double width;

    public Rectangle() {
        this.length = 1.0D;
        this.width = 1.0D;
    }

    public Rectangle(double width, double length) {
        this.length = length;
        this.width = width;
    }

    public Rectangle(double width, double length, String color, boolean filled) {
        this.length = length;
        this.width = width;
        this.color = color;
        this.filled = filled;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getLength() {
        return this.length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getWidth() {
        return this.width;
    }

    @Override
    public double getArea() {
        return this.width * this.length;
    }

    @Override
    public double getPerimeter() {
        return 2.0 * (this.width + this.length);
    }

    public String toString() {
        return "Rectangle[" + super.toString() + "],length = " + this.length + ", Width = " + this.width + "]";
    }

}
