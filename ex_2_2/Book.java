public class Book {
    private String name;
    private Author[] author;
    private double price;
    private int qty = 0;

    public Book(String name, Author[] author, double price) {
        this.name = name;
        this.author = author;
        this.price = price;
    }

    public Book(String name, Author[] author, double price, int qty) {
        this.name = name;
        this.author = author;
        this.price = price;
        this.qty = qty;
    }

    public String getName() {
        return this.name;
    }

    public Author[] getAuthor() {
        return this.author;
    }

    public Double getPrice() {
        return this.price;
    }

    public void setPrice(Double price) {
        this.price = price;
    }

    public void setQty(int qty) {
        this.qty = qty;
    }

    public int getQty() {
        return this.qty;
    }

    public String toString() {

        String s = "";

        for (Author xx : author) {
            s += xx.toString();
            s += ", ";
        }

        return "Book [name =" + getName() + " Authors = " + s + " price = " + getPrice() + " qty = " + getPrice() + "]";
    }
}
