public class Circle {

    private String color;
    private double radius;

    public Circle() {

        // System.out.println("Circle constructor");
        this.color = "red";
        this.radius = 1.0;
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public Circle(double radius, String color) {
        this.color = color;
        this.radius = radius;
    }

    public double getRadius() {
        return this.radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public String getColor() {
        return this.color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public String toString() {
        return "Circle[radius=" + this.radius + "," + " color=" + this.color + "]";
    }

    public double getArea() {
        return Math.PI * this.radius * this.radius;
    }

}
