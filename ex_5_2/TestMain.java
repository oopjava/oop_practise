public class TestMain {
    public static void main(String[] args) {

        // no argument constructor
        Cylinder c1 = new Cylinder();
        System.out.println(c1);
        System.out.println(c1.CylinderArea());

        // height constructor
        Cylinder c2 = new Cylinder(3.2);
        System.out.println(c2);
        System.out.println(c2.CylinderArea());

        // circle and height constructor
        Circle nC = new Circle(2.3, "black");
        Cylinder c3 = new Cylinder(nC, 4.9);
        System.out.println(c3);
        System.out.println(c3.CylinderArea());

        // color,radius,height
        Cylinder c4 = new Cylinder("white", 1.5, 3.3);
        System.out.println(c4);
        System.out.println(c4.CylinderArea());

    }
}
