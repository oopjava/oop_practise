public class Cylinder {
    private Circle base;
    private double height;

    public Cylinder() {
        this.base = new Circle(1.0, "blue");
        this.height = 1.0;
    }

    public Cylinder(double height) {
        this.base = new Circle(1.3, "yellow");
        this.height = height;
    }

    public Cylinder(Circle circle, double height) {
        this.base = circle;
        this.height = height;
    }

    public Cylinder(String color, double radius, double height) {
        this.base = new Circle(radius, color);
        this.height = height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    public double getHeight() {
        return this.height;
    }

    public void setRadius(double radius) {
        base.setRadius(radius);
    }

    public double getRadius() {
        return base.getRadius();
    }

    public double CylinderArea() {
        return (Math.PI * 2.0 * base.getRadius()) * (this.height + base.getRadius());
    }

    public String toString() {
        return "Cylinder[" + base.toString() + ",height=" + this.height + "]";
    }

}
