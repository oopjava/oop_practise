public class Circle {
    private double radius = 1.0;

    public Circle() {
    }

    public Circle(double radius) {
        this.radius = radius;
    }

    public void setRadius(double radius) {
        this.radius = radius;
    }

    public double getRadius() {
        return this.radius;
    }

    public double getArea() {
        return Math.PI * this.radius * this.radius;
    }

    public double getCircumference() {
        return 2.0 * Math.PI * this.radius;
    }

    public String toString() {
        return "Circle Radius : [" + this.radius + "]";
    }

    // public String toString() {
    // return "Circle Radius : " + this.radius + " Circle Area : " + getArea() + "
    // Circle Circumference : "
    // + getCircumference();
    // }
}
