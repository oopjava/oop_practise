public class TestCircle {
    public static void main(String[] args) {
        Circle c1 = new Circle();
        System.out.println("Radius: " + c1.getRadius() + " Area: " + c1.getArea());
        Circle c2 = new Circle(2.0);
        System.out.println("Radius: " + c2.getRadius() + " Area: " + c2.getArea());
        Circle c3 = new Circle(4.0,"green");
        System.out.println("Radius: " + c3.getRadius() + " Area: " + c3.getArea()+" color: "+c3.getColor());
 
        
        Circle c4 = new Circle();
        c4.setRadius(5.5);
        System.out.println(c4.getRadius());
        c4.setColor("Blue");
        System.out.println(c4.getColor());


        // toString Call
        System.out.println(c4.toString());


    }
}
