public class TestMain {
    public static void main(String[] args) {

        // only co-ordinates
        MyLine l1 = new MyLine(0, 0, 2, 2);
        System.out.println(l1);

        System.out.println(l1.getLength());
        System.out.println(l1.getGradient());

        // object type constructor
        MyPoint p1 = new MyPoint(2, 2);
        MyPoint p2 = new MyPoint(6, 6);

        MyLine l2 = new MyLine(p1, p2);
        System.out.println(l2);
        System.out.println(l2.getLength());
        System.out.println(l2.getGradient());

        
        l2.setEndXY(7, 9);
        System.out.println(l2.getLength());
        System.out.println(l2.getGradient());

    }
}