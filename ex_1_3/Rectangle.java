public class Rectangle {
    private float length;
    private float width;

    public Rectangle() {
        this.length = 1.0f;
        this.width = 1.0f;
    }

    public Rectangle(float length, float width) {
        this.length = length;
        this.width = width;
    }

    public void setLength(Float length) {
        this.length = length;
    }

    public double getLength() {
        return this.length;
    }

    public void setWidth(Float width) {
        this.width = width;
    }

    public float getWidth() {
        return this.width;
    }

    public double getArea() {
        return this.width * this.length;
    }

    public double getPerimeter() {
        return 2.0 * (this.width + this.length);
    }

    public String toString() {
        return "Rectangle [length = " + this.length + " Width = : " + this.width + "]";
    }

}
