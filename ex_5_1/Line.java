public class Line extends Point {
    private Point end;

    public Line(int beginX, int beginY, int endX, int endY) {
        super(beginX, beginY);
        this.end = new Point(endX, endY);
    }

    public Line(Point begin, Point end) {
        super(begin.getX(), begin.getY());
        this.end = end;
    }

    public Point getBegin() {
        Point c = new Point(super.getX(), super.getY());
        return c;
    }

    public void setBegin(Point begin) {
        super.setX(begin.getX());
        super.setY(begin.getY());
    }

    public Point getEnd() {
        Point c = new Point(this.end.getX(), this.end.getY());
        return c;
    }

    public void setEnd(Point end) {
        this.end = end;
    }

    public int getBeginX() {
        return super.getX();
    }

    public void setBeginX(int x) {
        super.setX(x);
    }

    public int getBeginY() {
        return super.getY();
    }

    public void setBeginY(int y) {
        super.setY(y);
    }

    public int getEndX() {
        return this.end.getX();
    }

    public void setEndX(int x) {
        this.end.setX(x);
    }

    public int getEndY() {
        return this.end.getY();
    }

    public void setEndY(int y) {
        this.end.setY(y);
    }

    public int[] getBeginXY() {
        int[] point = { super.getX(), super.getY() };
        return point;
    }

    public void setBeginXY(int x, int y) {
        super.setX(x);
        super.setX(y);
    }

    public int[] getEndXY() {
        int[] point = { this.end.getX(), this.end.getY() };
        return point;
    }

    public void setEndXY(int x, int y) {
        this.end.setX(x);
        this.end.setY(y);
    }

    public double getLength() {
        int p = Math.abs(super.getX() - this.end.getX());
        int q = Math.abs(super.getY() - this.end.getY());
        double r = Math.sqrt((p * p) + (q * q));
        return r;
    }

    public double getGradient() {
        double yDiff = this.end.getY() - super.getY();
        double xDiff = this.end.getX() - super.getX();
        double r = Math.atan2(yDiff, xDiff);
        return r;
    }

    public String toString() {
        return "Line[begin=" + super.toString() + ",end=(" + this.end.getX() + ","
                + this.end.getY() + ")";
    }

}
