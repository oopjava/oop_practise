public class Rectangle implements GeometricObject {
    private double length;
    private double width;

    public Rectangle(double width, double length) {
        this.length = length;
        this.width = width;
    }

    @Override
    public double getArea() {
        return this.width * this.length;
    }

    @Override
    public double getPerimeter() {
        return 2.0 * (this.width + this.length);
    }

    public String toString() {
        return "Rectangle[width = " + this.width + ", length = " + this.length + "]";
    }

}
