public class TestMain {
    public static void main(String[] args) {

        MyRectangle r1 = new MyRectangle(0, 2, 2, 0);
        System.out.println(r1);
        System.out.println(r1.getPerimeter());

        MyPoint left = new MyPoint(-2, 2);
        MyPoint right = new MyPoint(2, -2);

        MyRectangle r2 = new MyRectangle(left, right);
        System.out.println(r2);
        System.out.println(r2.getPerimeter());

    }
}