public class MyRectangle {
    private MyPoint left;
    private MyPoint right;

    public MyRectangle(int x1, int y1, int x2, int y2) {
        this.left = new MyPoint(x1, y1);
        this.right = new MyPoint(x2, y2);
    }

    public MyRectangle(MyPoint v1, MyPoint v2) {
        this.left = new MyPoint(v1.getX(), v1.getY());
        this.right = new MyPoint(v2.getX(), v2.getY());
    }

    public String toString() {
        return "MyRectangle[left=" + left.toString() + ",right=" + right.toString() + "]";
    }

    public double getPerimeter() {

        // anti clockwise
        MyPoint a = new MyPoint(left.getX(), right.getY());
        MyPoint b = new MyPoint(right.getX(), right.getY());
        MyPoint c = new MyPoint(right.getX(), left.getY());
        MyPoint d = new MyPoint(left.getX(), left.getY());

        double p = a.distance(b);
        double q = b.distance(c);

        return 2.0 * (p + q);
    }

}
