public class Rectangle extends Shape {
    private double length;
    private double width;

    public Rectangle() {
        this.length = 1.0d;
        this.width = 1.0d;
    }

    public Rectangle(double length, double width) {
        this.length = length;
        this.width = width;
    }

    public void setLength(double length) {
        this.length = length;
    }

    public double getLength() {
        return this.length;
    }

    public void setWidth(double width) {
        this.width = width;
    }

    public double getWidth() {
        return this.width;
    }

    public double getArea() {
        return this.width * this.length;
    }

    public double getPerimeter() {
        return 2.0 * (this.width + this.length);
    }

    public String toString() {
        return "Rectangle[" + super.toStirng() + ",]length = " + this.length + " Width = : " + this.width + "]";
    }

}
