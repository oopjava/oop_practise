public class Square extends Rectangle {
    public Square() {
        super();
    }

    public Square(double side) {
        super(side, side);
    }

    public void setLength(double side) {
        super.setLength(side);
    }

    public void setWidth(double width) {
        super.setWidth(width);
    }

    public String toString() {
        return "Square[" + super.toStirng() + "]";
    }

    public double getArea() {
        return super.getArea();
    }

    public double getPerimeter() {
        return super.getPerimeter();
    }
}