public class Time {
    private int hour;
    private int minute;
    private int second;

    public Time(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public int getHour() {
        return this.hour;
    }

    public int getMinute() {
        return this.minute;
    }

    public int getSecond() {
        return this.second;
    }

    public void setHour(int hour) {
        this.hour = hour;
    }

    public void setMinute(int minute) {
        this.minute = minute;
    }

    public void setSecond(int second) {
        this.second = second;
    }

    public void setTime(int hour, int minute, int second) {
        this.hour = hour;
        this.minute = minute;
        this.second = second;
    }

    public String toString() {
        return getHour() + ":" + getMinute() + ":" + getSecond();
    }

    public Time nextSecond() {
        this.second++;
        
        if (this.second == 60)
        {
            this.minute++;
            this.second = 0;
        }

        if (this.minute == 60)
        {
            this.hour++;
            this.minute = 0;
        }
        if (this.hour == 24)
        {
            this.hour = 0;
        }
        return (this);
    }

}
